from abc import ABC, abstractmethod
from typing import List

import cv2 as cv
import numpy as np
from skimage import morphology as morph


# Colour Science: https://github.com/colour-science/colour
from colour_checker_detection.detection.segmentation import adjust_image, WORKING_WIDTH, \
  colour_checkers_coordinates_segmentation
  
# Plant CV: https://github.com/danforthcenter/plantcv
from plantcv.plantcv.transform import find_color_card, create_color_card_mask

# Python macduff: https://github.com/mathandy/python-macduff
from python_macduff import find_macbeth

RgbaImg = RgbImg = BlackWhiteImg = XYVertices = np.ndarray

class CCDetector(ABC):
  """
  Interface for normalizing the behavior of all detection methods.
  Devised by Nathan Jessurun.
  """
  def __init__(self, name: str):
    self.name = name
    self.lastCCList: List[XYVertices] = [np.array([[]])]

  @abstractmethod
  def _findCCImpl(self, ccImg: RgbImg) -> List[XYVertices]:
    raise NotImplementedError

  def findCC(self, ccImg: RgbImg) -> List[XYVertices]:
    ccList = self._findCCImpl(ccImg)
    self.lastCCList = ccList
    return ccList

  def iouScore(self, ccImg: RgbImg, gtruthImg: BlackWhiteImg):
    toFill = np.zeros(gtruthImg.shape, dtype='uint8')
    detectedCcs = self.findCC(ccImg)
    cv.fillPoly(toFill, detectedCcs, 1)
    toFill = toFill > 0
    intersect = toFill & gtruthImg
    union = toFill | gtruthImg
    return intersect.sum()/union.sum()

  def lastCCToImg(self, imgShape: tuple) -> BlackWhiteImg:
    cvOut = np.zeros(imgShape[:2], 'uint8')
    cv.fillPoly(cvOut, self.lastCCList, 1)
    return cvOut.astype(bool)

class PlantCV(CCDetector):
  """
  Interface for Plant CV to behave the same as other detection methods.
  Adapted from regular Plant CV methods by Nathan Jessurun.
  """
  def __init__(self):
    super().__init__('Plant CV')

  def _findCCImpl(self, ccImg: RgbImg) -> List[XYVertices]:
    try:
      chipDf, start, spacing = find_color_card(ccImg)
      mask = create_color_card_mask(ccImg, 15, start, spacing, 4,6)
      locs = np.hstack([arr[:,None] for arr in np.nonzero(mask)])
      bounds = np.concatenate([arr.flatten() for arr in (np.min(locs, 0), np.max(locs, 0))])
      coords = bounds.flatten()
      ccBoundsList = [
        [coords[1], coords[0]],
        [coords[3], coords[0]],
        [coords[3], coords[2]],
        [coords[1], coords[2]]
      ]
      toReturn = [np.array(ccBoundsList)]
    except RuntimeError as ex:
      toReturn = []

    return toReturn

class ColourScience(CCDetector):
  """
  Interface for Colour Science to behave the same as other detection methods.
  Adapted from regular Colour Science methods by Nathan Jessurun and Olivia Paradis.
  """
  def __init__(self):
    super().__init__('Colour Science')

  def _findCCImpl(self, ccImg: RgbImg) -> XYVertices:
    imShape = ccImg.shape[:2]
    adjImgShape = np.array(adjust_image(ccImg, WORKING_WIDTH).shape[:2])
    ccBoundsLst = colour_checkers_coordinates_segmentation(ccImg)
    divFactor = adjImgShape / imShape
    for cc in ccBoundsLst:
      cc[:, 0] = np.round(cc[:, 0] / divFactor[1])
      cc[:, 1] = np.round(cc[:, 1] / divFactor[0])
    return ccBoundsLst

class Macduff(CCDetector):
  """
  Interface for Macduff to behave the same as other detection methods.
  Adapted from regular Macduff methods by Olivia Paradis.
  """
  def __init__(self):
    super().__init__('Macduff')

  def _findCCImpl(self, ccImg: RgbImg) -> List[XYVertices]:
    try:
      _, colorChecker = find_macbeth(ccImg.copy())
      swatchLocs = colorChecker.points.reshape(-1,2)
      corners = swatchLocs[[0, 5, -1, -6]].astype(int)
      smallMask = np.zeros(ccImg.shape[:2],dtype='uint8')
      cv.fillPoly(smallMask, [corners], 1)
      # Dilate to account for the fact that corners are in the center of outer
      # swatches
      dilateRad = int((colorChecker.size + colorChecker.size/4)//2)
      smallMask = morph.dilation(smallMask, morph.disk(dilateRad))
      # Now that a larger mask is obtained, convert back to corners
      tmpCorners,_ = cv.findContours(smallMask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
      corners = [tmpCorners[0][:,0,:]]
      return corners
    except cv.error:
      return []