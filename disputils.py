from pathlib import Path
from typing import List, Optional, Union

import numpy as np
import pyqtgraph as pg
from matplotlib import cm
from pyqtgraph.exporters import ImageExporter
from qimage2ndarray import rgb_view, alpha_view

RgbaImg = RgbImg = BlackWhiteImg = NChanImg = np.ndarray
pg.setConfigOption('imageAxisOrder', 'row-major')

class ImageCompositor:
  def __init__(self):
    # -----
    # Create properties
    # -----
    self.app = pg.mkQApp()
    self.viewbox = pg.ViewBox(invertY=True)
    self.win = pg.PlotWidget(viewBox=self.viewbox, show=False)
    self.winSz = self.win.size().width(), self.win.size().height()
    self.legend = pg.LegendItem(offset=(5,5), brush=[0,0,0])
    self.baseImgItem = pg.ImageItem()
    self.addedItems: List[pg.ImageItem] = []

    self.legendFontArgs = {'size': '13pt', 'color': 'w', 'bold': True, 'italic': False}

    # -----
    # Configure relationships
    # -----
    self.legend.setParentItem(self.win.plotItem)
    for ax in 'left', 'bottom', 'right', 'top':
      self.win.plotItem.hideAxis(ax)
    self.win.setContentsMargins(0, 0, 0, 0)
    self.baseImgItem.setZValue(1)
    self.viewbox.addItem(self.baseImgItem)

  def setBaseImg(self, baseImg: NChanImg, clearOverlays=True):
    self.winSz = baseImg.shape[:2][::-1]
    self.viewbox.setRange(
      xRange=[0, baseImg.shape[1]], yRange=[0, baseImg.shape[0]], padding=0
    )
    self.baseImgItem.setImage(baseImg)
    self.refreshWinContents()
    if clearOverlays:
      self.clearOverlays()

  def addMasks(self, masks: List[BlackWhiteImg], names: List[Optional[str]]=None, alpha=0.5,
               colors: list=None, removeBlank=True):
    backgroundClr = [0,0,0,0]
    if colors is None:
      colors = cm.get_cmap('viridis', len(masks)).colors*255
    if names is None:
      self.legend.hide()
      names = [None]*len(masks)
    else:
      self.legend.show()

    if removeBlank:
      # Take out masks that are totally blank. Iterate in reverse order to avoid
      # index modification of untouched elements during deletion
      # Waiting until here to remove blank masks ensures the colors
      # will stay the same for all names if several images are overlayed
      idxsToKeep = list(range(len(masks)))
      for ii in reversed(idxsToKeep):
        if not np.any(masks[ii]):
          for lst in masks, names, idxsToKeep:
            del lst[ii]
      # 'Colors' is a np array, so we can't delete from it in the same loop
      colors = colors[idxsToKeep,:]
      

    curZ = 2
    lastLegIdx = len(self.legend.items)
    for name, color in zip(names, colors):
      brush, pen = pg.mkBrush(color), pg.mkPen(color)
      curScat = pg.ScatterPlotItem(symbol='s', brush=brush, pen=pen, width=5)
      self.legend.addItem(curScat, name=name)
    self.setLegendFontStyle(lastLegIdx, **self.legendFontArgs)
    for mask, color in zip(masks, colors):
      curLut = np.array([backgroundClr, color])
      toAdd = pg.ImageItem(mask, levels=[0,1], lut=curLut)
      toAdd.setZValue(curZ)
      toAdd.setOpacity(alpha)
      self.addedItems.append(toAdd)
      self.viewbox.addItem(toAdd)
      curZ += 1
    self.refreshWinContents()

  def setLegendFontStyle(self, startItemIdx=0, **lblTxtArgs):
    for item in self.legend.items[startItemIdx:]:
      for single_item in item:
        if isinstance(single_item, pg.LabelItem):
          single_item.setText(single_item.text, **lblTxtArgs)

  def clearOverlays(self):
    for imgItem in self.addedItems:
      self.viewbox.removeItem(imgItem)
    self.addedItems = []
    self.legend.clear()

  def toNumpy(self) -> RgbaImg:
    exporter = ImageExporter(self.viewbox)
    outQArr = exporter.export(toBytes=True)
    return np.dstack((rgb_view(outQArr), alpha_view(outQArr)))

  def save(self, saveFile: Union[str, Path]) -> bool:
    exporter = ImageExporter(self.viewbox)
    success = exporter.export(saveFile)
    return success

  def refreshWinContents(self):
    # For some reason, processing events causes window size to screw up on export.
    # To fix this, every time win events are processed force another resize.
    # Find workaround?
    self.app.processEvents()
    self.win.resize(*self.winSz)
    self.viewbox.resize(*self.winSz)

  def show(self):
    self.win.show()
    self.app.exec()
