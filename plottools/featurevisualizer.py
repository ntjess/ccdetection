if __name__ == '__main__':
  import sys
  sys.path.append('..')

import inspect
from pathlib import Path
from tqdm import tqdm

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import convolve2d
from skimage import color
from skimage import io
from skimage.feature import corner_harris
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.preprocessing import StandardScaler

from prochelpers import getImg_maskFiles


class FeatureFuns:
  """
  Features interface that includes functions to calculate features from binary images
  Members:
  
  sob = np.array([[1,2,1], [0,0,0], [-1,-2,-1]])
  sob = sob + 1j*sob.T

  @staticmethod
  def harrisCorners(img):
    grayIm = color.rgb2gray(img)
    return corner_harris(grayIm)

  @staticmethod
  def sobelFilter(img):
    if img.ndim < 3:
      img = img[:,:,None]
    outPerChan = [convolve2d(img[:,:,ii], FeatureFuns.sob, 'same')[:,:,None] for ii in range(img.shape[2])]
    return np.abs(np.dstack(outPerChan))
  """

  @staticmethod
  def rgb(img):
    return img
colorFns = inspect.getmembers(color, inspect.isfunction)
for fnName, fn in colorFns:
  if 'rgb2' in fnName:
    setattr(FeatureFuns, fnName.replace('rgb2', ''), staticmethod(fn))


class FeatureVis:
  @classmethod
  def getFeatureVec(cls, img: np.array) -> np.array:
    # FEATURE EXTRACTION
    fnlist = inspect.getmembers(FeatureFuns, predicate=inspect.isfunction)

    allFeatures = []
    allFeatNames = []
    for ii, (fnName, featurefun) in enumerate(fnlist):
      fnval = featurefun(img)
      # Feature functions can return a list of names corresponding to each value
      if isinstance(fnval, tuple):
        curFeatNames, fnval = fnval
      else:
        # If no label is provided, use the function name
        numFeats = fnval.shape[2] if fnval.ndim > 2 else 1
        if numFeats == 1:
          curFeatNames = [fnName]
        else:
          curFeatNames = [f'{fnName}_{arrayIdx}' for arrayIdx in range(numFeats)]
        allFeatures.append(fnval.reshape(-1,numFeats))
      allFeatNames.extend(curFeatNames)
    return allFeatNames, np.hstack(allFeatures)

  @classmethod
  def visFeatures(cls, inFolder: Path):
    np.random.seed(42)
    dataFiles, labelFiles = [np.array(arr) for arr in getImg_maskFiles(inFolder)]

    keepIdxs = np.random.permutation(len(labelFiles))[1:100]
    labelFiles = labelFiles[keepIdxs]
    dataFiles = dataFiles[keepIdxs]

    imgs = [io.imread(str(file)) for file in dataFiles]
    labelImgs = [io.imread(str(file), as_gray=True) for file in labelFiles]
    
    labelArr = np.vstack([np.reshape(img, (-1,1)) for img in labelImgs])
    featureLst = []

    for img in tqdm(imgs):
      featureNames, curFeats = cls.getFeatureVec(img)
      featureLst.append(curFeats)
    featureMat = np.vstack(featureLst)
    visTool = LDA()
    scaler = StandardScaler()
    print('Normalizing data...')
    featureMat = scaler.fit_transform(featureMat, labelArr)
    print('Performing LDA...')
    visTool.fit(featureMat, labelArr.flatten())
    print('Formatting for display...')
    avgLdaCoefMag = np.mean(np.abs(visTool.coef_), axis=0)
    sortOrder = np.argsort(avgLdaCoefMag)
    avgLdaCoefMag = avgLdaCoefMag[sortOrder] / avgLdaCoefMag.sum()
    featureNames = np.array(featureNames)[sortOrder]
    thresh = 0.03
    otherVals = avgLdaCoefMag[avgLdaCoefMag <= thresh].sum()
    valsToPlot = np.append(avgLdaCoefMag[avgLdaCoefMag >= thresh], otherVals)
    valLabels = np.append(featureNames[avgLdaCoefMag >= thresh], 'other')
    fig, ax = plt.subplots()
    ax.pie(valsToPlot, autopct='%2.f', labels=valLabels, radius=0.75,
           textprops={'fontsize': 10})
    ax.set_title('Average magnitude of feature importance after LDA (%)')
    return fig, ax

if __name__ == '__main__':
  #base = Path('../RepresentativeImgs/GroundTruth/NNData')
  plt.switch_backend('qt5agg')
  base = Path('/home/UFAD/njessurun/Downloads/NNData')
  curFig, curAx = FeatureVis.visFeatures(base)
  curFig.show()
  curFig.savefig('./ldaPlot.pdf', bbox_inches='tight', pad_inches=0)
  breakp = 1
