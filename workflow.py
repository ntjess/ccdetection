from pathlib import Path
from typing import List
from tqdm import tqdm

import pyqtgraph as pg
from matplotlib import cm
from matplotlib.colors ListedColormap

import numpy as np
from pandas import DataFrame as df
import pandas as pd
from skimage import io
from skimage.color import label2rgb

from ccdetectorinterface import CCDetector, ColourScience, PlantCV, Macduff
from prochelpers import mkImgToGTruthMapping, overlayDetections, getImg_maskFiles

pg.setConfigOption('imageAxisOrder', 'row-major')


def showWorking(img: np.ndarray, maskImg: np.ndarray):
  """
  Code to run an interactive demo for presentation purposes.
  Idea designed by Olivia Paradis, code by Nathan Jessurun.
  """
  ccDetectionMethods: List[CCDetector] = [ColourScience(), PlantCV(), Macduff()]
  colorsToShow = cm.get_cmap('viridis', len(ccDetectionMethods) + 1)
  
  fig, ax = plt.subplots()
  ax.imshow(img)
  
  # Ground truth
  curToShow = colorsToShow.colors[0]
  curToShow[3] = 0.5
  ax.imshow(maskImg, cmap=ListedColormap([[0,0,0,0], curToShow]))
  ax.scatter([], [], c=[curToShow], label='Ground Truth')
  ax.legend()
  fig.show()
  
  for ii, detector in enumerate(ccDetectionMethods):
    # Other methods
    curToShow = colorsToShow.colors[ii+1]
    curToShow[3] = 0.5
    detector.findCC(img)
    ax.imshow(detector.lastCCToImg(img.shape), cmap=ListedColormap([[0,0,0,0], curToShow]))
    ax.scatter([], [], c=[curToShow], label=detector.name)
    ax.legend()
    fig.show()


def main(imgFolder: Path, outFolder: Path):
  """
  Reads in input images and calculates IOU scores and colour checker locations for each
  detection method. Designed by Nathan Jessurun.
  Logic for computing and storing color checker coordinates devised by Olivia Paradis.
  """
  outOverlayFolder = outFolder/'Overlays'
  outOverlayFolder.mkdir(exist_ok=True, parents=True)
  iouThresh = 0.05

  ccFiles, maskFiles = getImg_maskFiles(imgFolder)
  ccDetectionMethods: List[CCDetector] = [ColourScience(), PlantCV(), Macduff()]
  iouScores = np.empty([len(ccFiles), len(ccDetectionMethods)])
  bboxes = np.empty([len(ccFiles), len(ccDetectionMethods)], dtype=object)

  for ii in tqdm(range(len(ccFiles)), 'IOU File'):
    imgFile = ccFiles[ii]
    maskFile = maskFiles[ii]
    curRow = []
    curImg = io.imread(str(imgFile))[:,:,:3]

    curGTruth = io.imread(str(maskFile))
    overlayDetections(ccDetectionMethods, curImg, 0.5, curGTruth,
                      saveFile=str(outOverlayFolder/imgFile.name))
    for jj, method in enumerate(ccDetectionMethods):
      method: CCDetector
      score = method.iouScore(curImg, curGTruth)
      coords = [curCoords.tolist() for curCoords in method.lastCCList]
      iouScores[ii,jj] = score
      bboxes[ii,jj] = coords

  outFolder.mkdir(parents=True, exist_ok=True)
  for curArr, outName in zip([iouScores, bboxes], ['iou', 'coords']):
    imNames = [Path(file).name for file in ccFiles]
    curTbl = df(curArr, columns=[method.name for method in ccDetectionMethods])
    curTbl = curTbl.set_index([imNames])
    outFile = outFolder / f'{outName}.csv'
    curTbl.to_csv(str(outFile))
  
if __name__ == '__main__':
  base = Path('./InputImgs/')
  # base = Path('/home/UFAD/njessurun/Downloads')
  outFolder = Path('./Results')
  main(base, outFolder)
